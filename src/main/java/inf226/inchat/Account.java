package inf226.inchat;
import inf226.util.immutable.List;
import inf226.util.Pair;


import inf226.storage.*;

/**
 * The Account class holds all information private to
 * a specific user.
 **/
public final class Account {
    /*
     * A channel consists of a User object of public account info,
     * and a list of channels which the user can post to.
     * TODO: This might be a good place to store the data for the
     * user.
     */
    public final Stored<User> user;
    public final List<Pair<String,Stored<Channel>>> channels;
    
    public Account(Stored<User> user, 
                   List<Pair<String,Stored<Channel>>> channels) {
        this.user = user;
        this.channels = channels;
    }
    
    /**
     * Create a new Account.
     *
     * @param user The public User profile for this user.
     * @param password The login password for this account.
     **/
    public static Account create(Stored<User> user,
                                 String password) {
        // TODO: The password parameter is not used for anything.
        return new Account(user,List.empty());
    }
    
    /**
     * Join a channel with this account.
     *
     * @return A new account object with the cannnel added.
     */
    public Account joinChannel(String alias, Stored<Channel> channel) {
        Pair<String,Stored<Channel>> entry
            = new Pair<String,Stored<Channel>>(alias,channel);
        return new Account
                (user,
                 List.cons(entry,
                           channels));
    }


    /**
     * Check weather if a string is a correct password for
     * this account.
     *
     * @return true if password matches.
     */
    public boolean checkPassword(String password) {
        // TODO: This does not seem right.
        return true;
    }
    
    
}
